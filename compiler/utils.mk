# Script for Programming system generation
pdir:
	mkdir -p bin

build-ppl: pdir
	gcc -o bin/komppl compiler/sources/komppl.c

build-asm: pdir
	gcc -o bin/kompassr compiler/sources/kompassr.c

build-debug: pdir
	gcc -o bin/absloadm compiler/sources/absloadm.c -lncurses

clear: 
	rm -Rf compiler/bin
