docker_container = ubuntu-14
docker_workdir = /home/tmp/
docker_args = --rm \
	-w ${docker_workdir} \
	-v ${PWD}:${docker_workdir}

docker-build-container:
	@docker build -t ${docker_container} -f deployment/Dockerfile . 

docker-%:
	@echo "Important: command exec in docker container ${docker_container}"
	@echo "Run arguments ${args}"
	@docker run -it ${docker_args} ${docker_container} make ${*}
	@echo "Terminate container ${docker_container}"

docker-bash:
	@echo "Important: open bash in docker container ${docker_container}"
	@docker run -it ${docker_args} ${docker_container} bash ${args}
	@echo "Terminate container ${docker_container}"
